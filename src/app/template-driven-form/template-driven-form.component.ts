import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.css']
})
export class TemplateDrivenFormComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit (data) {
      console.warn(data);
  }

  userData = {
    "email": "ankur@gmail.com",
    "mobile": 900200003,
    "address": "R P Path"
  }

}
