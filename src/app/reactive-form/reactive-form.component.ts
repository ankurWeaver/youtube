import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup, FormGroupName } from '@angular/forms';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  // ========Video 39===================
  loginForm = new FormGroup({
      email: new FormControl('ankurbardhan2019@gmail.com'),
      password: new FormControl('gita4200'),
  });

  collectData () {
      console.warn(this.loginForm.value);
  }

  //========Video 40 (with validation)===================
  loginForm2 = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
  });

}
