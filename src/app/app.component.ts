import { Component } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { TestService } from './test.service';
import { PostService } from './post.service';

@Component({
   selector: 'app-root',
   templateUrl: './app.component.html',
   styleUrls: ['./app.component.css']
})
export class AppComponent {
   title = 'youtube';
   
   

   //======= video 13 (*forms) ===========
   constructor(
      private fb: FormBuilder, 
      private http: HttpClient, 
      t1: TestService,
      private postStr: PostService 
      ) {
      
      //======= video 20 (service) ===========
      t1.printCase();
   }

   



   //======= video 15 (Pass Data From Parent to Child Component) ===========
   heros = ["suparman", "Batman", "Antman"];

   changeHeros () {
      this.heros = ["Tarzen", "Vikrant", "Simba"];
   }

   

   

   
   

}
