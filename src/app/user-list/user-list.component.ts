import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { PostService } from '../post.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(
    private fb: FormBuilder, 
    private http: HttpClient, 
    private postStr: PostService 
  ) { 
    this.tutInfo = this.fb.group({
      user: ['', Validators.required],
      address: ['', [Validators.required, Validators.minLength(5)]]
   });
  }
  

  //======= video 16 (Api Calling) ===========
  apiUrl = "https://jsonplaceholder.typicode.com/users";
  apiData:any;
  ngOnInit () {
     this.http.get(this.apiUrl).subscribe((data)=>{
        
        this.apiData = data;
     });
  }

  //======= video 6 (Interpolation) ===========
  title = 'youtube';
  obj = {
    name: "Ankur",
    email: "ankur@gmail.com"
  }

  //======= video 7 (Click Event) ===========
  clickEvent() {
     alert("Function Called");
  }

  //======= video 7 (Click Event) ===========
  getValues(val) {
     console.warn(val.target.value);
  }

  //======= video 8 (Property Binding) ===========
  isDisabled = false;

  //======= video 9 (*ngIf) ===========
  show = false

  //======= video 11 (*ngSwitch) ===========
  color = "green1";

  //======= video 12 (*ngFor) ===========
  arrColor = ["red", "blue", "green", "yellow"];
  
  //======= video 13 (*forms) ===========
  tutInfo: any;

  onSubmit(values) {
    if (this.tutInfo.status != 'INVALID') {
       console.log("form submitted", values);
    } else {
       alert("Proper Input Not Submitted");
    }
  }

  get user() {
    return this.tutInfo.get('user');
  }

  get address() {
      return this.tutInfo.get('address');
  }

  //======video 17 (Style Binding) ============
  colorCode = "green";
  errorStyle = true;

   //======= video 22 (Pipe) ===========
   fullname = "anil kumar";
   birthday = new Date();
   
   dataFullname:string = "Ankur";

   //======= video 22 (Post Api call with service) ===========
   allUserData = [];
   GetUserAll () {
       this.postStr.getPostData().subscribe((data)=>{
           console.log(data);
           this.allUserData = data.result;
       });
   }

   //======= video 18 (Post Api With Form Submit) ===========
   postApiUrl = 'http://localhost:3600/users/signup';
   restoForm = new FormGroup({
      password: new FormControl(''),
      email: new FormControl(''),
   });

   callApiPost () {
      console.warn(this.restoForm.value);
      this.http.post(this.postApiUrl, this.restoForm.value).subscribe((data)=>{
            console.warn(data);
      });
   }


  

  
}
