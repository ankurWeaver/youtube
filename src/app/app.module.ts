import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserListComponent } from './user-list/user-list.component';
import { UsersModule } from './users/users.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ChildComponentComponent } from './child-component/child-component.component';
import { HttpClientModule } from '@angular/common/http';
import { TestService } from './test.service';
import { PostService } from './post.service';
import { ReversepipePipe } from './reversepipe.pipe';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { TemplateDrivenFormComponent } from './template-driven-form/template-driven-form.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';


@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    ChildComponentComponent,
    ReversepipePipe,
    PageNotFoundComponent,
    HomeComponent,
    UserDetailsComponent,
    TemplateDrivenFormComponent,
    ReactiveFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UsersModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    
  ],
  providers: [
    TestService,
    PostService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
